# Web Examples

## Continuous Integration (ci)

`ci-jenkins` is an example of Jenkins CI system.

To use	`ci-jenkins`, use `docker-compose up` and then access it by
`localhost:8080`. Visit the official Jenkins website for tutorials.

Note that the Jenkins docker has been given privilege to summon other dockers.
This allows dockerised pipeline builds. It also will not stop when the server
reboots.

## VPN

An OpenVPN server example is located in `vpn-openvpn`. It uses the openvpn docker.

## Scala

A scala-sbt with docker example is in `scala-sbt`. See the project README for
details.

## Python

A python package import example is given in `python-package` which solves
the relative import problem.

## Mobile Development (mobile)

In `native-frontend/`, there is an example of NativeScript application with
Angular. See the project README for details.
