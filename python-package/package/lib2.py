import os

def show():
    print(f"[lib2.show]\n"
          f"pwd: {os.getcwd()}\n"
          f"file: {__file__}")

if __name__ == '__main__':
    show()

