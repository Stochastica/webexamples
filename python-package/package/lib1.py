import os
from . import lib2

def call_lib2():
    print(f"[lib1.call_lib2]\n"
          f"pwd: {os.getcwd()}\n"
          f"file: {__file__}")
    lib2.show()

if __name__ == '__main__':
    call_lib2()

