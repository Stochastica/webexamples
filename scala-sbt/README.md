# Scala-sbt Example

## Creation

The project was created by
```
./exec sbt new scala/scala-seed.g8
```
and type in `webex`. Do not press any other keys before typing the name since
sbt will record them and this causes encoding problems.

## Execution

Note that:
1. The `/root` directory in the docker is linked to `./cache`
2. To use sbt, use
```
$ ./exec.sh bash
root@docker# cd scala-sbt
root@docker# sbt
sbt> ~run
```
3. To test,
```
$ ./exec.sh bash
root@docker# cd scala-sbt
root@docker# sbt
sbt> test
```

## IntelliJ IDEA integration

There is currently no way of letting IntelliJ IDEA operate entirely inside the
docker, so sbt has to be used in conjuncation of IDEA. To import the project:

1. Open the `scala-sbt/webex` directory (not `scala-sbt`) in IntelliJ
2. If any loading failure happens, use `File - Invalidate Caches / Restart ...`
