#!/bin/bash

IMAGE="hseeberger/scala-sbt:15.0.1_1.4.5_2.12.12"

docker run \
		--name webex-ex-scalasbt --rm -it \
		-v "${PWD}:/data" \
		-v "${PWD}/cache:/root/" \
		-v '/etc/localtime:/etc/localtime:ro' \
		--workdir='/data' \
		--entrypoint "$1" \
		$IMAGE \
		"${@:2}"
