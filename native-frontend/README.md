# NativeScript FrontEnd Example

This showcases a simple NativeScript application. The content was created by [tns create](https://docs.nativescript.org/tooling/docs-cli/project/creation/create) on the `hello-world` template with Angular.

## Setup

### iOS

Note that iOS development has to be done on a macOS machine.

The following must be installed. The entire development environment takes more than 10GB so space must be reserved on the disk. **Follow the instructions very carefully since missing a single step could cause cryptic compilation errors.**

1. A [node.js](https://nodejs.org/) LTS version (`node@14`)
2. `sudo npm install --global nativescript`
3. (**Slow**) XCode (note that XCode must be run at least once before you can use it)
4. iOS simulator (XCode - Preferences - Components) for iOS~14.2 (Another version would work fine).
5. Ruby headers: `brew install rbenv ruby-build`. (**Slow**) Then `rbenv install 2.6.3`
  This workaround was found in [StackOverflow](https://stackoverflow.com/questions/53135863/macos-mojave-ruby-config-h-file-not-found) to resolve installation problems with the next step.
6. The [CocoaPods](https://github.com/CocoaPods/CocoaPods) library: **Under the exported environment variables in `setup.sh`**, execute `sudo gem update --system; sudo gem install -n /usr/local/bin xcodeproj cocoapods`.

  A plain `sudo gem install cocoapods` may or may not work.

Use `tns doctor` to spot any warnings.

To test if the installation has been successful
```sh
tns prepare ios
tns run ios --emulator
```

## Troubleshooting


### Building for iOS Simulator ...

The example does not work out-of-the-box. When running `tns run ios --emulator`, we have
```
error: Building for iOS Simulator, but the linked and embedded framework 'TNSSideDrawer.framework' was built for iOS + iOS Simulator. ...
```
The solution is to add the following line to `App_Resources/iOS/build.xcconfig`: (provided [here](https://github.com/NativeScript/nativescript-cli/issues/5449#issuecomment-746365706))
```
VALIDATE_WORKSPACE = YES;
```
